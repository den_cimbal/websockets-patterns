import { getTextId } from "./gameHelper";

export const createCommentIntroduce = () => "Здравствуйте, меня зовут Денис и сегодня я буду комментировать этот супер сложный 100 км заезд начинающих гонщиков!!!";

export const createCommentPresentation = (players) => {
  const playersList = players
    .map((player, index) => `<p>${index + 1}. ${player.username} - ${player.car}</p>`)
    .join("\n");
  return `Встречайте участников этого заезда громкими авациями ${playersList}`;
};

export const createCommentCurrentResults = (players) => {
  const currentPlayers = [...players];
  currentPlayers.sort((a, b) => b.progress - a.progress);
  const currentPlayersList = currentPlayers
    .map((player, index) => `<p>${index + 1}. ${player.username} - только что преодолел ${player.progress} км </p>`)
    .join("\n");
  return `И вот настал долгожданный момент) Сейчас мы покажем вам промежуточные результаты: ${currentPlayersList}`;
};

export const createCommentFinish = (username) => {
  return `Гонщик ${username} финишировал!!!`;
};

export const createCommentResults = (players) => {
  return `Результаты: ${players} А сейчас Техперсонал приводит в надлежащее состояние дорогу и машины. И через несколько секунд мы сможем начинать новый заезд!!!`;
};

export const createCommentAlmostFinish = (players) => {
  const currentPlayers = [...players];
  currentPlayers.sort((a, b) => b.progress - a.progress);
  const currentPlayersList = currentPlayers
    .map((player, index) => `<p>${index + 1}. ${player.username} - только что преодолел отметку ${player.progress} км </p>`)
    .join("\n");
  return `Итак. Почти финиш и на данный момент у нас вот такая турнирная таблица: ${currentPlayersList}`;
};

export const createCommentRandom = () => {
  const randomComments = [
    "В поселке Выпивохино началась очередная гонка по формуле C2H5OH. Лучшие гонщики уже гонят вовсю и находятся на втором этапе перегонки.",
    "И было у старика Шумахера три сына: двое гонщиков, а третий — водитель маршрутки.",
    "Средняя стоимость болида составляет 7 миллионов долларов.",
    "А тем временем гонщик 'Формулы-1' в первую брачную ночь пришел к жене пятым.",
    "Средний возраст наших гонщиков - 39 лет.",
    "А в это время сборная Украины по футболу обыгрывает сборную Швеции и отправляется в четвертьфинал. Давайте поздравим наших футболистов!!! Но не отвлекаемся от гонки."
  ];
  const randomCommentId = getTextId(randomComments.length);
  return randomComments[randomCommentId];
};
