import { addUser, deleteUser } from "./helpers/userHelper";
import { createRoom, joinToTheRoom, leaveRoom, changeReadyStatus, showRooms, updateLeavingRooms } from "./helpers/roomHelpers";
import { updateGameProgress } from "./helpers/gameHelper";

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const userId = socket.id;
    const { error } = addUser(username, userId);

    if(error) {
      socket.emit("USER_EXIST", error);
    }

    showRooms(socket);

    socket.on("CREATE_ROOM", (roomName) => {
      createRoom(roomName, username, socket, io);
    });

    socket.on("JOIN_ROOM", (roomName) => {
      joinToTheRoom(roomName, username, socket, io);
    });

    socket.on("LEAVE_ROOM", (roomName) => {
      leaveRoom(roomName, username, socket, io);
    });

    socket.on("CHANGE_READY_STATUS", (roomName) => {
      changeReadyStatus(roomName, username, socket, io);
    });

    socket.on("UPDATE_GAME_PROGRESS", ({ roomName, updatedProgress, charsLeft }) => {
      updateGameProgress(roomName, username, updatedProgress, charsLeft);
    });

    socket.on("disconnect", () => {
      deleteUser(userId);
      updateLeavingRooms(username, socket, io);
    });
  });
};
