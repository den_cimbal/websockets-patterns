let gameRoom;
let timeToGameEnd;
let currentСharIndex = 0;
let gameText;
let gameSocket;
let progress;
let isGameOver = false;

export const renderPrepareToGameView = () => {
  const leaveRoomButton = document.getElementById("quit-room-btn");
  const readyButton = document.getElementById("ready-btn");
  const commentator = document.getElementById("commentator");

  leaveRoomButton.classList.add("display-hidden");
  readyButton.classList.add("display-none");
  commentator.classList.remove("display-none");
};

export const renderGameView = () => {
  const gameStartTimer = document.getElementById("countdown");
  const textContainer = document.getElementById("text-container");

  gameStartTimer.classList.add("display-none");
  textContainer.classList.remove("display-none");
};

export const updateGameStartTimer = (timeToGameStart) => {
  const gameStartTimer = document.getElementById("countdown");
  gameStartTimer.classList.remove("display-none");
  gameStartTimer.innerHTML = timeToGameStart;
};

export const updateGameTimer = (timeToGameEnd) => {
  const gameTimer = document.getElementById("timer");
  gameTimer.classList.remove("display-none");
  gameTimer.innerHTML = `${timeToGameEnd} seconds left`;
};

export const updateEnteredText = (textElements) => {
  const textContainer = document.getElementById("text-container");
  textContainer.innerHTML = "";
  textContainer.append(...textElements);
};

export const updateGameText = (text, currentIndex, userChar) => {
  const currentTextChar = text.charAt(currentIndex);
  if (currentTextChar === userChar) {
    const enteredText = text.slice(0, currentIndex);
    const correctTextContainer = document.createElement("span");
    const updatedText = enteredText + userChar;
    correctTextContainer.innerText = updatedText;
    const notEnteredText = text.slice(currentIndex + 1);

    if (notEnteredText.length) {
      const notEnteredTextContainer = document.createElement("span");
      notEnteredTextContainer.innerText = notEnteredText;
      notEnteredTextContainer.classList.add("text__left");
      correctTextContainer.classList.add("text__correct");
      updateEnteredText([correctTextContainer, notEnteredTextContainer]);
    } else {
      correctTextContainer.classList.add("text__correct--full");
      updateEnteredText([correctTextContainer]);
    }
    return true;
  }
  return false;
};

export const updateUserProgress = (text, currentIndex) => {
  return Math.round(100 / (text.length / currentIndex));
};


export const handleGame = (event) => {
  const isTextUpdated = updateGameText(gameText, currentСharIndex, event.key);
  if (isTextUpdated) {
    currentСharIndex++;
    const charsLeft = gameText.length - currentСharIndex;
    progress = updateUserProgress(gameText, currentСharIndex);
    gameSocket.emit("UPDATE_GAME_PROGRESS", { roomName: gameRoom, updatedProgress: progress, charsLeft });
  }
};

export const startGame = () => {
  document.addEventListener("keydown", handleGame);
  renderGameView(timeToGameEnd);
  const updateTimeToGameEnd = () => {
    updateGameTimer(timeToGameEnd);
    timeToGameEnd--;
    if (isGameOver || timeToGameEnd < 0) {
      clearInterval(timerId);
    }
  };
  updateTimeToGameEnd();
  const timerId = setInterval(updateTimeToGameEnd, 1000);
};


export const gameStartTimer = (timeToGameStart, startGame) => {
  let currentTime = timeToGameStart;
  const updateTime = () => {
    updateGameStartTimer(currentTime);
    currentTime--;
    if (currentTime < 0) {
      clearInterval(timerId);
      startGame();
    }
  };
  updateTime();
  const timerId = setInterval(updateTime, 1000);
};

export const getText = async (textId) => {
  const response = await fetch(`/game/texts/${textId}`);
  return await response.text();
};

export const updateText = (text) => {
  const textContainer = document.getElementById("text-container");
  textContainer.innerHTML = text;
};

export const prepareToGame = (timeToGameStart, roomName, textId, gameTime, socket) => {
  gameRoom = roomName;
  timeToGameEnd = gameTime;
  gameSocket = socket;
  isGameOver = false;
  currentСharIndex = 0;

  renderPrepareToGameView(timeToGameStart);
  gameStartTimer(timeToGameStart, startGame);
  getText(textId).then(text => {
    updateText(text);
    gameText = text;
  });
};

export const renderRoomUser = (user) => {
  const username = sessionStorage.getItem("username");
  const { progress, isReady } = user;

  const userWrapper = document.createElement("li");
  userWrapper.classList.add("user");
  const userName = document.createElement("p");
  userName.classList.add("user-title");
  const progressBarWrapper = document.createElement("div");
  progressBarWrapper.classList.add("progress-wrapper");
  const progressBar = document.createElement("div");
  progressBar.classList.add("progress-bar", `${user.username.replace(/\s/g, '-')}`);

  userName.innerHTML = user.username == username ? `${user.username} (you)` : user.username;
  progressBar.style.width = `${progress}%`;
  if (progress == 100) {
    progressBar.classList.add("progress-bar--full");
  }
  progressBarWrapper.append(progressBar);

  if (isReady) {
    userWrapper.classList.remove("ready-status-red");
    userWrapper.classList.add("ready-status-green");
  } else {
    userWrapper.classList.add("ready-status-red");
    userWrapper.classList.remove("ready-status-green");
  }

  userWrapper.innerHTML = "";
  userWrapper.append(userName, progressBarWrapper);
  return userWrapper;
};

export const renderRoomUsersList = (users) => {
  const userList = document.getElementById("users-list");
  const usersElements = users.map((user) => renderRoomUser(user));
  userList.innerHTML = "";
  userList.append(...usersElements);
};

export const renderRoomPage = (room) => {
  const leaveRoomButton = document.getElementById("quit-room-btn");
  const roomHeader = document.getElementById("room-title");
  const readyButton = document.getElementById("ready-btn");
  const countdown = document.getElementById("countdown");
  const timer = document.getElementById("timer");
  const text = document.getElementById("text-container");
  const commentator = document.getElementById("commentator");
  const comment = document.getElementById("comment");

  roomHeader.innerHTML = room.roomName;
  readyButton.innerHTML = "Ready";
  countdown.innerHTML = "";
  timer.innerHTML = "";
  text.innerHTML = "";
  comment.innerHTML = "";

  renderRoomUsersList(room.users);
  readyButton.classList.remove("display-none");
  leaveRoomButton.classList.remove("display-hidden");
  countdown.classList.add("display-none");
  timer.classList.add("display-none");
  text.classList.add("display-none");
  commentator.classList.add("display-none");
};

export const endGame = (updatedRoom) => {
  isGameOver = true;
  document.removeEventListener("keydown", handleGame);
  setTimeout(() => renderRoomPage(updatedRoom), 10000);
};

export const updateComment = (newComment) => {
  const comment = document.getElementById("comment");
  comment.innerHTML = newComment;
};