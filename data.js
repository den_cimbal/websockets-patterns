export const texts = [
  "The steps include installing a react-native-firebase package by using the command 'yarn add react-native-firebase', adding Firebase cocoa pod, and running it.  Then create a new firebase project, for iOS from Xcode copy bundle identifier and paste on Firebase, download, and add it back to Xcode at a specific location",
  "During the week, Nate Brakeley works as a data analyst. But on the weekends, he competes with Rugby United New York, the city’s first professional major league rugby franchise, now in its fourth season",
  "Crowds gathering in stadiums, pubs and bars to watch the European Championship soccer games have driven a rise in coronavirus cases across Europe, the World Health Organization said on Thursday, raising concerns about another virus wave  even though vaccination campaigns have made progress",
  "The rise in cases linked to the tournament comes more than a year after soccer games hosted early last year led to some of the first outbreaks in Europe",
  "Germany's interior minister, Horst Seehofer, called the decision by European's soccer governing body, UEFA, which runs the tournament, to allow large crowds in stadiums utterly irresponsible",
  "Whoever is up first makes coffee, which is usually Emily. I roll out of bed and get an egg, sausage and cheese on an everything bagel from International Bagel and Cafe, which is around the corner. Emily does intermittent fasting so she doesn’t eat breakfast",
  "At 6 a.m. Wednesday, Bujold's lawyer called to tell her that she had won an appeal that would make her eligible to compete in Tokyo beginning in July. Her hopes were in jeopardy because of the pandemic and changes in the qualifying rules that effectively disqualified Bujold because she had a child",
];

export default { texts };
